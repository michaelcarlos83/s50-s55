import CourseCard from '../components/CourseCard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'
// import courses_data from '../data/courses' mock data No longer needed

export default function Courses(){
//.map creates a new array to iterate through the data 
	const[courses, setCourses] = useState([])
	const[isLoading, setIsLoading] = useState(false)

	useEffect((isLoading) => {
		//sets the loading state to true
		setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(result => {
			setCourses(
				result.map(course => {
							return (
							<CourseCard key={course._id} course={course}/>
						)
					})
			       )
			//Sets the loading state to false
			setIsLoading(false)

 	        })
    }, [])

			return(
				
					(isLoading === true) ?

						<Loading/>
						
						:

						<>
						{courses}		
						</>				
			)
		}





