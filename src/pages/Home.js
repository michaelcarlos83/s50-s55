import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home(){

	return(
		<>
			<Banner/>
       	    <Highlights/>

       	    {/*
				Activity 

				1. Create a courses component with a bootstrap card inside of it 
				2. the courses component must have a title description price and button to enroll 
				3. import the courses component to the home.js file and successfully display it upon page reload/recompilation 
       	    */}
		</>
	)
}