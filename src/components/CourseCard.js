import {useState, useEffect} from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';   
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

//props.course.name/price to access the data coming from the database
//props - syntax code for declaring a props parameter
//course - assigned value from the main source
//.name/.price - refers to the specific key value pairs from data base
//({course}) - to shorten that code instead of using props.course.keyvalue

export default function CourseCard({course}) {

  const [slots, setSlots] = useState(15)

  //initialize a count state with a value of (0)
  const [count, setCount] = useState(0)

  const [isOpen, setIsOpen] = useState(true)


  const {name, description, price, _id} = course
  /*function enroll (){
    if(slots > 0){
      setCount(count + 1)
      setSlots(slots - 1);
    }
    else{
      alert('Slots for this course is full')
      
    }
    
  }
*/
  //effects in react is just like side effects/effects in real life, where everything something happens withoin the componen, a function or condition runs

  //You may also listen or watch a specific for change instead of watching/listening to the whole component

/*  useEffect(() => {
    if (slots === 0){
      setIsOpen(false)
    }


  }, [slots])*/

  return (
    <Card style={{ width: '100%' }}>
      <Card.Img variant="top"/>
      <Card.Body className="cardCourse">
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>
          <strong>Description:</strong>
        </Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>
          <strong>Price:</strong>
        </Card.Subtitle>
        <Card.Text>
          <strong>PHP {price}</strong>
        </Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  );
}

//PropTypes can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.

CourseCard.propTypes  = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}



